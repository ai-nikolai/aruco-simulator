/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"
#include <opencv2/opencv.hpp>
#include "aruco.h"
#include "cvdrawingutils.h"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/core/types_c.h"

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 640



bool change = false;
bool detect = false;

static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		change = !change;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_A && action == GLFW_PRESS)
		detect = !detect;
}


/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(WINDOW_WIDTH, WINDOW_HEIGHT,"Simulation!", &window, false);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}

	window_obj -> use();
	window_obj -> enable_depth();
	window_obj -> set_BGD(0.2f, 0.3f, 0.3f);
	window_obj -> draw();

	std::cout << window_obj -> depth_enabled << std::endl;

	glew_init();

	shader_program shaderprogram("vertex3d.vsh", "fragment3d.fsh");
	shaderprogram.use();






	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/

	GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };



	/*********************************************************************************************
	* Texture
	**********************************************************************************************/
	int tex_height, tex_width;


	GLuint texture[6];
	glGenTextures(6, texture);

	for(int i = 0;i<6;i++){
		glBindTexture(GL_TEXTURE_2D, texture[i]);

		//some important parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	}

	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao;
	GLuint vbo[3];



	//generating VBO for vertex data
	glGenBuffers(3, vbo);

	//binding the VAO for attributes
	glGenVertexArrays(1, &vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");
	GLuint texAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "tex_coord");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


	glBindVertexArray(vao);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Position attribute
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(posAttrib);
    // TexCoord attribute
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(texAttrib);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);











	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/

	GLuint model_location = glGetUniformLocation(shaderprogram.shaderprogram, "model");
	GLuint view_location = glGetUniformLocation(shaderprogram.shaderprogram, "view");
	GLuint projection_location = glGetUniformLocation(shaderprogram.shaderprogram, "projection");



	glm::mat4 view;
	glm::mat4 projection; //camera parameters



	view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
	projection = glm::perspective(45.0f, (float)(window_obj -> width) / (float)(window_obj -> height), 0.1f, 100.0f); //!!!!important

	glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection));

	/*
	* Key Callback
	*/

    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2); //escape only







	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* ARUCO Set_UP
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/

	//aruco variables
	aruco::CameraParameters camera_parameters;
	aruco::MarkerDetector marker_detector;
	std::vector< aruco::Marker >  markers;

	/**************************************
	*for CameraParams
	**************************************/
	//camera
	cv::Mat camera_matrix = (cv::Mat1d(3, 3) << projection[0][0], 0, 0, 0, projection[1][1], 0, 0, 0, 1);
	std::cout << camera_matrix << "managed" << std::endl;

	//Distorsion
	cv::Mat distorsion_matrix = (cv::Mat1d(4,1) << 0,0,0,0);

	//Scree Size
	CvSize::CvSize screen_size_cv = CvSize::CvSize(WINDOW_WIDTH,WINDOW_HEIGHT);

	//setting the params
	camera_parameters.setParams(camera_matrix,distorsion_matrix,screen_size_cv);

	/**************************************
	*for MarkerDetector
	**************************************/
	//threshold Params
	marker_detector.setThresholdParams(7, 7);
	marker_detector.setThresholdParamRange(2, 0);


	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/

	//matrices
	glm::mat4 model_template;
	glm::mat4 model;

	//opencv
	cv::namedWindow("OpenCV Window", CV_WINDOW_NORMAL);
	cv::resizeWindow("OpenCV Window", WINDOW_WIDTH,WINDOW_HEIGHT);

	cv::Mat framebuffer,flipped;
	cv::Mat id[6];

	flipped.create(window_obj->height, window_obj->width, CV_8UC3);

	id[0] = cv::imread("id=1.png",  CV_LOAD_IMAGE_COLOR);
	id[1] = cv::imread("id=2.png",  CV_LOAD_IMAGE_COLOR);
	id[2] = cv::imread("id=3.png",  CV_LOAD_IMAGE_COLOR);
	id[3] = cv::imread("id=4.png",  CV_LOAD_IMAGE_COLOR);
	id[4] = cv::imread("id=5.png",  CV_LOAD_IMAGE_COLOR);
	id[5] = cv::imread("id=30.png", CV_LOAD_IMAGE_COLOR);

	for(int i = 0;i<6;i++){
		glBindTexture(GL_TEXTURE_2D, texture[i]);

		glTexImage2D(GL_TEXTURE_2D,     	// Type of texture
					 0,                 	// Pyramid level (for mip-mapping) - 0 is the top level
					 GL_RGB,            	// Internal colour format to convert to
					 id[i].cols,          	// Image width  i.e. 640 for Kinect in standard mode
					 id[i].rows,         	 // Image height i.e. 480 for Kinect in standard mode
					 0,                 	// Border width in pixels (can either be 1 or 0)
					 GL_BGR, 				// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
					 GL_UNSIGNED_BYTE,  	// Image data type
					 id[i].ptr());        	// The actual image data itself

	}


	//other things
	float temp_v=0, prev_v=0, diff_v = glfwGetTime();
	//main loop
	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();


		/***************************
		* Rotation
		***************************/
		float time_v = glfwGetTime();
		std::cout <<"time: "<<time_v;
		diff_v = time_v - prev_v;
		std::cout <<" rotational offset: " << diff_v << std::endl;
		prev_v = time_v;


		if(!change){
			temp_v += diff_v;
			model = glm::rotate(model_template,temp_v, glm::vec3(0.5f, 1.0f, 0.0f));
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));
		}


		/***************************
		* Drawing
		***************************/
		//empty the screen
		window_obj -> set_BGD(0.2f, 0.3f, 0.3f);

		//drawing different sides
		for(int j = 0; j<6 ; j++){
			glBindTexture(GL_TEXTURE_2D, texture[j]);
			//drawing the elements
			glDrawArrays(GL_TRIANGLES, j*6, 6);
		}
		window_obj->draw();


		/***************************
		* OPENCV AND ARUCO
		***************************/
		//Transfer from OpenGL to OpenCV
		glReadPixels(0, 0, flipped.cols, flipped.rows, GL_BGR, GL_UNSIGNED_BYTE, flipped.data);
		cv::flip(flipped,framebuffer,0);

		if(detect){
			float marker_size = 0.5;
			marker_detector.detect(framebuffer, markers,camera_parameters,marker_size);
		}
		for (int i = 0; i < markers.size(); i++) {
			std::cout << markers[i] << std::endl;
			markers[i].draw(framebuffer, CvScalar(0, 0, 255), 2);
		}

		cv::imshow("OpenCV Window", framebuffer);

	}





	printf("done\n");




	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(3, vbo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
