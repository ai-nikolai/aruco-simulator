@Author: Nikolai Rozanov <Nikolai>
@Date:   02-09-2016
@Email:  nikolai.rozanov@gmail.com
@Last modified by:   Nikolai
@Last modified time: 02-09-2016
@ DOES NOT WORK!!!



cmake_minimum_required(VERSION 3.0)

project(test)

find_package(OpenCV REQUIRED)

ADD_EXECUTABLE(test test.cpp)

target_link_libraries(${PROJECT_NAME} PUBLIC ${OpenCV_LIBS})
