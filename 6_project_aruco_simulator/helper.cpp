/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   23-09-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/

/*********************
*Description:
    This file is with additional Variables and all user input handling
*********************/

/*********************
*Window Definitions
*********************/
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 640


/*********************
*Keys
*********************/
bool keys[1024];


/*********************
*Controls
*********************/
bool rotate_ = false;
bool detect = false;


/*********************
*Rotation
*********************/
float main_r=0, diff_r=0.01, change_r=0.001;

glm::vec3 rotation_vec = glm::vec3(1.0f,0.5f,0.0f), prev_rotation;

prev_rotation = glm::rotate(prev_rotation,0.0f,glm::vec3(1.0f,0.0f,0.0f));



/*********************
*Camera
*********************/
glm::vec3 cameraPos   	= glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront 	= glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    	= glm::vec3(0.0f, 1.0f,  0.0f);
glm::vec3 cameraRight 	= glm::vec3(1.0f, 0.0f,	 0.0f);


GLfloat cameraSpeed = 0.03f;



static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{


    //close window
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    //aruco
    if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS)
		detect = !detect;
    //rotation
    if (key == GLFW_KEY_R && action == GLFW_PRESS){
        diff_r = -diff_r;
        change_r = -change_r;
    }
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        rotate_ = !rotate_;
    //printing
    if (key == GLFW_KEY_P && action == GLFW_PRESS){
        std::cout << "Rotation -> Angle: " << main_r << " Vec: " << glm::to_string(rotation_vec) << std::endl;
    }

	if (key >= 0 && key < 1024){
        if (action == GLFW_PRESS)
            keys[key] = true;
        else if (action == GLFW_RELEASE)
            keys[key] = false;
    }
}

void do_actions(){

	//speed
	if (keys[GLFW_KEY_F])
		cameraSpeed += 0.002;
	if (keys[GLFW_KEY_H])
		cameraSpeed -= (0.002>cameraSpeed)?0.002:(cameraSpeed/2.0f);


	// Camera controls
	if (keys[GLFW_KEY_W])
		cameraPos += cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_S])
		cameraPos -= cameraSpeed * cameraFront;
	if (keys[GLFW_KEY_A])
		cameraPos -= cameraSpeed * cameraRight;
	if (keys[GLFW_KEY_D])
		cameraPos += cameraSpeed * cameraRight;
	if (keys[GLFW_KEY_UP])
		cameraPos += cameraSpeed * cameraUp;
	if (keys[GLFW_KEY_DOWN])
		cameraPos -= cameraSpeed * cameraUp;

    //rotation
    if (keys[GLFW_KEY_1]){
        rotation_vec = glm::vec3(1.0f,0.0f,0.0f);
        prev_rotation
    }
    if (keys[GLFW_KEY_2]){
        rotation_vec = glm::vec3(0.0f,1.0f,0.0f);
    }
    if (keys[GLFW_KEY_3]){
        rotation_vec = glm::vec3(0.0f,0.0f,1.0f);
    }
    if (keys[GLFW_KEY_4]){
        rotation_vec = glm::vec3(1.0f,0.5f,0.0f);
    }

    if (keys[GLFW_KEY_T])
        diff_r += change_r;
    if (keys[GLFW_KEY_Y])
        diff_r -= change_r;


}
