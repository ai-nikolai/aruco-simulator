/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/


//opencv
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/types_c.h>
//aruco
#include <aruco.h>
#include <cvdrawingutils.h>
//private
#include "drl.hpp"
#include "base.hpp"
//glm
#include "ext.hpp"
//helper
#include "helper.cpp"



/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(WINDOW_WIDTH, WINDOW_HEIGHT,"Simulation!", &window, false);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}

	window_obj -> use();
	window_obj -> enable_depth();
	window_obj -> set_BGD(0.2f, 0.3f, 0.3f);
	window_obj -> draw();

	std::cout << window_obj -> depth_enabled << std::endl;

	glew_init();

	shader_program shaderprogram("./shaders/vertex3d.vsh", "./shaders/fragment3d.fsh");
	shaderprogram.use();






	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/

	drl::Parser cube("./vertices/cube.in");
	drl::Parser floor("./vertices/floor.in");


	/*********************************************************************************************
	* Texture
	**********************************************************************************************/
	int tex_height, tex_width;


	GLuint texture[6], floor_texture;
	glGenTextures(6, texture);
	glGenTextures(1, &floor_texture);


	for(int i = 0;i<6;i++){
		glBindTexture(GL_TEXTURE_2D, texture[i]);

		//some important parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	}


	//
	glBindTexture(GL_TEXTURE_2D, floor_texture);
	//some important parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao[2];
	GLuint vbo[2];



	//generating VBO for vertex data
	glGenBuffers(2, vbo);

	//binding the VAO for attributes
	glGenVertexArrays(2, vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");
	GLuint texAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "tex_coord");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


	glBindVertexArray(vao[0]);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*cube.elements_read, cube.data, GL_STATIC_DRAW);

	// Position attribute
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(posAttrib);
    // TexCoord attribute
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(texAttrib);


	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);




	//floor
	glBindVertexArray(vao[1]);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*floor.elements_read, floor.data, GL_STATIC_DRAW);

	// Position attribute
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(posAttrib);
	// TexCoord attribute
	glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(texAttrib);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);


	glBindVertexArray(0);






	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/

	GLuint model_location = glGetUniformLocation(shaderprogram.shaderprogram, "model");
	GLuint view_location = glGetUniformLocation(shaderprogram.shaderprogram, "view");
	GLuint projection_location = glGetUniformLocation(shaderprogram.shaderprogram, "projection");



	glm::mat4 view;
	glm::mat4 projection; //camera parameters



	view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
	projection = glm::perspective(1.0f, (float)(window_obj -> width) / (float)(window_obj -> height), 0.1f, 100.0f); //!!!!important

	glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection));

	/*
	* Key Callback
	*/

    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2); //escape only







	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* ARUCO Set_UP
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/

	//aruco variables
	aruco::CameraParameters camera_parameters;
	aruco::MarkerDetector marker_detector;
	std::vector< aruco::Marker >  markers;

	/**************************************
	*for CameraParams
	**************************************/
	//camera
	cv::Mat camera_matrix = (cv::Mat1d(3, 3) << projection[0][0], 0, 0, 0, projection[1][1], 0, 0, 0, 1);
	std::cout << camera_matrix << "managed" << std::endl;

	//Distorsion
	cv::Mat distorsion_matrix = (cv::Mat1d(4,1) << 0,0,0,0);

	//Scree Size
	CvSize::CvSize screen_size_cv = CvSize::CvSize(WINDOW_WIDTH,WINDOW_HEIGHT);

	//setting the params
	camera_parameters.setParams(camera_matrix,distorsion_matrix,screen_size_cv);

	/**************************************
	*for MarkerDetector
	**************************************/
	//threshold Params
	marker_detector.setThresholdParams(7, 7);
	marker_detector.setThresholdParamRange(2, 0);




	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* OpenCV SET_UP
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/

	//opencv
	cv::namedWindow("OpenCV Window", CV_WINDOW_NORMAL);
	cv::resizeWindow("OpenCV Window", WINDOW_WIDTH,WINDOW_HEIGHT);

	cv::Mat framebuffer,flipped;
	cv::Mat id[6], floor_id;

	flipped.create(window_obj->height, window_obj->width, CV_8UC3);

	id[0] = cv::imread("./images/id=1.png",  CV_LOAD_IMAGE_COLOR);
	id[1] = cv::imread("./images/id=2.png",  CV_LOAD_IMAGE_COLOR);
	id[2] = cv::imread("./images/id=3.png",  CV_LOAD_IMAGE_COLOR);
	id[3] = cv::imread("./images/id=4.png",  CV_LOAD_IMAGE_COLOR);
	id[4] = cv::imread("./images/id=5.png",  CV_LOAD_IMAGE_COLOR);
	id[5] = cv::imread("./images/id=6.png", CV_LOAD_IMAGE_COLOR);

	floor_id = cv::imread("./images/brick.jpg");


	for(int i = 0;i<6;i++){
		glBindTexture(GL_TEXTURE_2D, texture[i]);

		glTexImage2D(GL_TEXTURE_2D,     	// Type of texture
					 0,                 	// Pyramid level (for mip-mapping) - 0 is the top level
					 GL_RGB,            	// Internal colour format to convert to
					 id[i].cols,          	// Image width  i.e. 640 for Kinect in standard mode
					 id[i].rows,         	 // Image height i.e. 480 for Kinect in standard mode
					 0,                 	// Border width in pixels (can either be 1 or 0)
					 GL_BGR, 				// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
					 GL_UNSIGNED_BYTE,  	// Image data type
					 id[i].ptr());        	// The actual image data itself

	}

	//floor
	glBindTexture(GL_TEXTURE_2D, floor_texture);
	glTexImage2D(GL_TEXTURE_2D,     	// Type of texture
				 0,                 	// Pyramid level (for mip-mapping) - 0 is the top level
				 GL_RGB,            	// Internal colour format to convert to
				 floor_id.cols,          	// Image width  i.e. 640 for Kinect in standard mode
				 floor_id.rows,         	 // Image height i.e. 480 for Kinect in standard mode
				 0,                 	// Border width in pixels (can either be 1 or 0)
				 GL_BGR, 				// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
				 GL_UNSIGNED_BYTE,  	// Image data type
				 floor_id.ptr());        	// The actual image data itself




	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************

	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************

	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/

	//matrices
	glm::mat4 model_template;
	glm::mat4 model, no_rotation;

	//no rotation or translation
	no_rotation = glm::rotate(model_template, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	prev_rotation = glm::rotate(model_template,0.0f,glm::vec3(0.0f,0.0f,1.0f));

	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();
		do_actions();

		/***************************
		* Movement
		***************************/
		//view
		view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));

		/***************************
		* Rotation
		***************************/


		if(!rotate_){
			main_r += diff_r;
			model = glm::rotate(model_template,main_r, rotation_vec);
			model = model * prev_rotation;
		}



		/***************************
		* Drawing
		***************************/
		//empty the screen
		window_obj -> set_BGD(0.2f, 0.3f, 0.3f);

		//drawing cube
		glBindVertexArray(vao[0]);
		glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));

		for(int j = 0; j<6 ; j++){
			glBindTexture(GL_TEXTURE_2D, texture[j]);
			//drawing the elements
			glDrawArrays(GL_TRIANGLES, j*6, 6);
		}
		//drawing floor
		glBindVertexArray(vao[1]);
		glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(no_rotation));

		glBindTexture(GL_TEXTURE_2D, floor_texture);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		window_obj->draw();

		glBindVertexArray(0);


		/***************************
		* OPENCV AND ARUCO
		***************************/
		//Transfer from OpenGL to OpenCV
		glReadPixels(0, 0, flipped.cols, flipped.rows, GL_BGR, GL_UNSIGNED_BYTE, flipped.data);
		cv::flip(flipped,framebuffer,0);

		//aruco
		if(detect){
			float marker_size = 0.5;
			marker_detector.detect(framebuffer, markers,camera_parameters,marker_size);

			for (int i = 0; i < markers.size(); i++) {
				//std::cout << markers[i] << std::endl;
				markers[i].draw(framebuffer, CvScalar(0, 0, 255), 2);
			}
		}

		//displaying second window
		cv::imshow("OpenCV Window", framebuffer);

	}





	printf("done\n");




	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(2, vao);
    glDeleteBuffers(3, vbo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
