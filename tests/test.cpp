/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   20-09-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include <base.hpp>
#include <opencv2/opencv.hpp>
#include "ext.hpp"


int main(){
    glm::mat4 projection = glm::perspective(45.0f, 1.0f, 0.1f, 100.0f);
    cv::Mat test_cv1, test_cv2;

    for(int i = 0; i<4 ; i++){
        //std::cout << glm::to_string(projection[i]) << std::endl;
        for(int j = 0; j<4 ; j++){
            //Mat cameraMatrix = (Mat1d(3, 3) << fx, 0, cx, 0, fy, cy, 0, 0, 1);
            std::cout << projection[i][j] << " ";
        }
        std::cout << std::endl;
    }
}
