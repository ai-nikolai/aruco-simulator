/********************************************************
*********************************************************
 @Author: Nikolai Rozanov <Nikolai>
 @Date:   29-08-2016
 @Email:  nikolai.rozanov@gmail.com

 (C) Nikolai Rozanov
*********************************************************
*********************************************************/



#include "base.hpp"
#include "soil.h"
#include "opencv2/opencv.hpp"





bool change = false;
bool other 	= false;

static void key_callback2(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		change = !change;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
		other = !other;

}


/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
* main function
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************/
int main(){


	/*********************************************************************************************
	* Initialising stuff
	**********************************************************************************************/
	glfw_init();

	GLFWwindow * window;
	GLFW_window * window_obj = new GLFW_window(640, 640,"First Window!", &window);

	if (window == *(window_obj -> window)){
		std::cout<<"yeyyy :))!";
	}

	window_obj -> use();
	window_obj -> enable_depth();
	window_obj -> set_BGD(0.2f, 0.3f, 0.3f);
	window_obj -> draw();

	std::cout << window_obj -> depth_enabled << std::endl;

	glew_init();

	shader_program shaderprogram("vertex3d.vsh", "fragment3d.fsh");
	shaderprogram.use();







	/*********************************************************************************************
	**********************************************************************************************
	* DATA:
	**********************************************************************************************
	**********************************************************************************************/

	GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };



	/*********************************************************************************************
	* Texture
	**********************************************************************************************/
	int tex_height, tex_width;


	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);


	//some parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);



	/*********************************************************************************************
	* OpenGl buffer object init
	**********************************************************************************************/


	GLuint vao;
	GLuint vbo[3];
	GLuint ibo;



	//generating VBO for vertex data
	glGenBuffers(3, vbo);

	//generating ibo for index data
	glGenBuffers(1, &ibo);

	//binding the VAO for attributes
	glGenVertexArrays(1, &vao);



	GLuint posAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "position");
	GLuint colAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "color");
	GLuint texAttrib = glGetAttribLocation(shaderprogram.shaderprogram, "tex_coord");



	/*********************************************************************************************
	* OpenGl buffer object specification
	**********************************************************************************************/


	glBindVertexArray(vao);

	//copying vertex data
	glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Position attribute
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(posAttrib);
    // TexCoord attribute
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(texAttrib);



	glEnableVertexAttribArray(posAttrib);
	glEnableVertexAttribArray(texAttrib);











	/*********************************************************************************************
	* Initialising drawing stuff
	**********************************************************************************************/

	GLuint model_location = glGetUniformLocation(shaderprogram.shaderprogram, "model");
	GLuint view_location = glGetUniformLocation(shaderprogram.shaderprogram, "view");
	GLuint projection_location = glGetUniformLocation(shaderprogram.shaderprogram, "projection");



	glm::mat4 view;
	glm::mat4 projection;

	view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
	projection = glm::perspective(45.0f, (float)(window_obj -> width) / (float)(window_obj -> height), 0.1f, 100.0f);

	glUniformMatrix4fv(view_location, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, glm::value_ptr(projection));

	/*
	* Key Callback
	*/

    //input output
	glfwSetKeyCallback(*(window_obj->window), key_callback2); //escape only







	/*********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************
	* Main loop
	**********************************************************************************************
	**********************************************************************************************
	**********************************************************************************************/


	//matrices
	glm::mat4 model_template;
	glm::mat4 model;

	//opencv
	cv::VideoCapture cap;
	cv::Mat frame;
	cv::Mat image;
	image = cv::imread("marker2.jpg", CV_LOAD_IMAGE_COLOR);



	if(!cap.open(0)){
		std::cout<<"ERROR VIDEO DID NOT OPEN"<<std::endl;
		return 0;
	}





	while (!glfwWindowShouldClose(*(window_obj->window))){

		glfwPollEvents();


		float time_v = glfwGetTime();
		std::cout <<"time: "<<time_v << "\n";


		//camera input
		cap >> frame;
		if( frame.empty() ){
			std::cout<<"ERROR VIDEO DID NOT Copy"<<std::endl;
			break; // end of video stream
		 }

		 glTexImage2D(GL_TEXTURE_2D,     	// Type of texture
					  0,                 	// Pyramid level (for mip-mapping) - 0 is the top level
					  GL_RGB,            	// Internal colour format to convert to
					  frame.cols,          	// Image width  i.e. 640 for Kinect in standard mode
					  frame.rows,          	// Image height i.e. 480 for Kinect in standard mode
					  0,                 	// Border width in pixels (can either be 1 or 0)
					  GL_BGR, 				// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
					  GL_UNSIGNED_BYTE,  	// Image data type
					  frame.ptr());





		if(!change){
			model = glm::rotate(model_template,time_v, glm::vec3(0.5f, 1.0f, 0.0f));
			glUniformMatrix4fv(model_location, 1, GL_FALSE, glm::value_ptr(model));
		}

		if(other){
			glTexImage2D(GL_TEXTURE_2D,     	// Type of texture
						 0,                 	// Pyramid level (for mip-mapping) - 0 is the top level
						 GL_RGB,            	// Internal colour format to convert to
						 image.cols,          	// Image width  i.e. 640 for Kinect in standard mode
						 image.rows,          // Image height i.e. 480 for Kinect in standard mode
						 0,                 	// Border width in pixels (can either be 1 or 0)
						 GL_BGR, 				// Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
						 GL_UNSIGNED_BYTE,  	// Image data type
						 image.ptr());        	// The actual image data itself
		}


		//empty the screen
		window_obj -> set_BGD(0.2f, 0.3f, 0.3f);

		//drawing the elements
		glDrawArrays(GL_TRIANGLES, 0, 36);

		window_obj->draw();


	}





	printf("done\n");







	/*********************************************************************************************
	* Ending all processes
	**********************************************************************************************/


	glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(2, vbo);
    glDeleteBuffers(1, &ibo);

    delete window_obj;
	glfw_destroy();



	return 0;
}
