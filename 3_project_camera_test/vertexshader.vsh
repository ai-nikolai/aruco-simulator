#version 330 core

in vec3 position;
in vec3 color;
in vec2 tex_coord;

out vec3 Color;
out vec2 TexCoord;

uniform mat4 transform_mat;

void main(){

	TexCoord = tex_coord;
	Color = vec3(1.0f,1.0f,1.0f) - color;
	gl_Position = transform_mat * vec4(position*2, 1.0f);
}
